var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Happy Foodie Restaurant' });
});

router.get('/home', function(req, res, next) {
  res.render('index');
});

router.get('/Contactus', function(req, res, next) {
  res.render('contacts');
});

router.get('/about', function(req, res, next) {
  res.render('about');
});

router.get('/list', function(req, res, next) {
  res.render('List');
});

module.exports = router;
